**allows you to make thumbnails (caps, previews) of video files. Supports of practically any videos-formats**
* [x] Upload output image (thumbnail) to any image webhosting service
* [x] Very fast
* [x] Easy to use
* [x] Possibility to save each shot in separate file
* [x] Load and save options presets
* [x] Autoadjustment of height of a shot
* [x] Customizable fonts, colors, contours, shadows, frames, background, text
* [x] Stamp time in shots
* [x] Video information stamp
* [x] Substitute black frames

`Download:`
[https://github.com/jamesheck2019/DT-Video-Thumbnailer/releases](https://github.com/jamesheck2019/DT-Video-Thumbnailer/releases)


![https://i.postimg.cc/43hVBGP5/DT-Video-Thumbnailer.png](https://i.postimg.cc/43hVBGP5/DT-Video-Thumbnailer.png)

![https://i.postimg.cc/Kz1Gnc5G/ure-Wiz796.png](https://i.postimg.cc/Kz1Gnc5G/ure-Wiz796.png)